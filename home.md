# Wiki de documentação da DT3 #

### Sobre o site DT3Sports ###

Novos recursos:

* FlexPanel - 13 Abas para editar a página de produto (Use a cor de fundo que combine com a a imagem)
* SwiperSlider - Slider de fotos de produtos e da home adaptados para touchscreen
* Imagens de diferentes tamanhos carregadas conforme a resolução - de celular a 4K
* O tamanho recomendado de imagens com largura total para produtos é 3840x1200
* O tamanho recomendado de imagens para os textos institucionais é 960x500
* Exibição de imagens de produtos 3D com Marmoset Viewer - necessário upload de arquivos .mview
* Shortcode para tabelas extensas, criando um efeito acordeon. Exemplo de uso: 

```
[tabela]
```

First Header  | Second Header
------------- | -------------
Content Cell  | Content Cell
Content Cell  | Content Cell

```
[fim-tabela] 
```

* Carrinho lateral permitindo alterar cores e calcular frete sem recarregar a página
* Blog listando os últimos vídeos do canal
* Cadastro e login através de redes sociais
* Menu de produtos editável por arrastar-e-soltar
* Conteúdo multilingue
* Avaliações e comentários na página de produto
* Álbum de produtos por cores. Os produtos devem ficar agrupados conforme a sua cor. As fotos recebem zoom ao se passar o mouse. Nos dispositivos mobile é habilitado o pinch-to-zoom.
* Shortcode para listar Revendas. Exemplo de uso:

```
[revenda url="http://revenda.com.br" logo="endereco-da-imagem.jpg"]
```

### Como corrigir falha de autentitcação do Bitbucket no Windows ###

De acordo com o link: [Falha de autenticação do Bitbucket](https://github.com/Microsoft/Git-Credential-Manager-for-Windows/issues/710), basta alterar a URL de origen de HTTPS para SSH. Exemplo:
```
git@bitbucket.org:desenvolvedordt3sports/dt3wiki.git
```

Para que a senha fique salva no Cmder acesse .ssh e execute o comando:
```
start-ssh-agent
```
